# scaphandre-runner

🌳 Demonstrates that we can measure of power consumption of a test run, by executing [Scaphandre](https://github.com/hubblo-org/scaphandre) in CI/CD.

- [scaphandre-runner](#scaphandre-runner)
  - [Principle](#principle)
  - [Setup](#setup)
    - [Install and register Gitlab runner on a physical machine](#install-and-register-gitlab-runner-on-a-physical-machine)
    - [Configure the runner (mount host volumes))](#configure-the-runner-mount-host-volumes)
    - [Configure the Gitlab project (CI settings)](#configure-the-gitlab-project-ci-settings)
      - [Required CI variables](#required-ci-variables)
    - [Filtering results](#filtering-results)
  - [Limitations](#limitations)

## Principle

⚡ We want to show that it is possible to measure the amount of electrical energy required to run a test suite in CI/CD.

However, the power monitoring agent do not play well with virtualized environments provided by public cloud vendors.

💡 This repository demonstrates using a specific _deported_ gitlab runner. This runner is installed **on a physical host** to be able to measure power usage.

In a real scenario we would trigger a complete test suite of the application. But for this demo we do not execute a real tests suite. 

Instead we generate artificial load using `stress-ng` in various configurations.
See [Ubuntu Manpage: stress-ng - a tool to load and stress a computer system](http://manpages.ubuntu.com/manpages/bionic/man1/stress-ng.1.html)


The GitalbCI script contacts the deported test runner (agent) and executes a 2 step job.

**Step 1**: measure energy during test:

1. runner starts scaphandre measure
2. runner waits for 5s (so that we can have values for a mostly idle system)
3. runner launch `stress-ng`
4. scaphandre stops measuring and returns results as a json file
5. results are saved as a job artifact (available in gitlab UI)

**Step 2**: filter results and save them to an external storage:

- calculate a average of instant power use (W) of the process under observation, multiply result by test duration to get _energy_ (J) consummed during the test.
- uploads it to carbon crush API (with details about the CI job, like the current branch)

See deatails in the GitlabCI script (`.gitlab-ci.yml`)

## Setup

Scaphandre can run in a container for CI purpose, but still needs access to the underlying physical host to measure Power consumption.

To allow this, you have to configure a gitlab runner _on a physical host_, tag it as `scaphandre` and register it in your Gitlab project.

### Install and register Gitlab runner on a physical machine

```sh
# Download install a gitlab runner
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
# Interactive configuration and registration
sudo gitlab-runner register
# or register with params
sudo gitlab-runner register \
     --non-interactive \
     --url https://gitlab.com \
     --registration-token __REDACTED__ \
     --name my-scaphandre-runner \
     --tag-list scaphandre \
     --locked \
     --paused \
     --executor docker \
     --docker-image ruby:2.6
```

### Configure the runner (mount host volumes)

When run in scaphandre using docker in CLI, we launch it with `docker run -v /sys/class/powercap:/sys/class/powercap -v /proc:/proc -ti hubblo/scaphandre stdout -t 15`. This implies that Scaphandre needs to get access to the files located in `/sys/class/powercap` and `/proc` of the docker host. 


On the gitlab runner host, we need to do the same. Edit the runner configuration (`/etc/gitlab-runner/config.toml`) to mount `/sys/class/powercap` and `/proc`

Add `/sys/class/powercap` and `/proc:/proc` to existing volume configuration

```sh
# Inside /etc/gitlab-runner/config.toml
# Replace
volumes = ["/cache"]
# by
volumes = ["/cache /sys/class/powercap:/sys/class/powercap /proc:/proc"]
```

### Configure the Gitlab project (CI settings)

The CI runner uses a custom docker image: [GitHub - demeringo/scaphandre-node-ci: A docker image of scaphandre and node to be used in CI](https://github.com/demeringo/scaphandre-node-ci)

- Ensure that gitlab project is configured to use this runner
- The use of the tag `scaphandre` in .gitlab-ci.yml ensures that the job runs on _this_ runner.

#### Required CI variables

The following variables need to be set on the Gitlab CI project.
See [Add a CI/CD variable to a project](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project) if you are not familiar with gitlab.

| variable name             | content                                                                                                | example value                                                        |
| -------------------- | ------------------------------------------------------------------------------------------------------ | -------------------------------------------------------------------- |
| CARBON_CRUSH_API_URL | the URL of the API where data should be uploaded after the test                                        | `https://ah577i9276.execute-api.eu-west-1.amazonaws.com/dev/measure` |
| CARBON_CRUSH_API_KEY | the secret key used to authenticate against the API (remember to **mask variable** in gitlab settings) | `AZCLOIKHJSDQSDFHDSQJQF`                                             |
| CARBON_CRUSH_APP_ID  | a unique identifier of the application under test (used by Carbon Crush API)                           | `my-app-123`                                                         |

### Filtering results

Results are filtered and formatted for upload using [scaph2cc](https://github.com/demeringo/scaph2cc).

scaph2C reads scaphandre json repots. It filters scaphandre results on a specific process name (`test-ng`) and calculate average power and total energy used during the test. 


### Visualization

Access grafana dashboard: <https://carboncrush.grafana.net>

## Limitations

- ⚠ The CI script filters the results on the process name (to measure only the impact of the test). This is currently *hardcoded* in the ci script (`.... select(.exe | contains("stress-ng"))...`).
- ⏱ The power measurement agent (Scaphandre) is will run only  for a maximum amount of time (this is configured through `--timeout 600` parameter in ci script. Adapt this setting to **ensure the measure lasts longer than your test run**.
- You need a runner on a **physical host** (or supported/specifically configured VM). Results are relative to _this_ physical architecture, so in case you have several runners, do not mix runners of different types if you need to compare results (🍏🍊)
- To be confirmed: the runner docker executor may need to run with `privileged` mode.
